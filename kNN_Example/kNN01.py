from numpy import *
import operator


def createDataSet():
    """
    一个简单的测试例子。
    :return:
    """
    # group是一个简单的特征数据集
    group = array([[1.0, 1.1], [1.0, 1.0], [0, 0], [0, 0.1]])
    # labels是上面的各个特征最终所对应的类型
    labels = ['A', 'A', 'B', 'B']
    # 通过classify函数来判断[0, 0]这个特征所属类型
    print('分类器求得的最终类型是：%s' % (classify([0.0], group, labels, 3)))


def classify(inX, dataSet, labels, k):
    """
    分类器，通过距离计算公式，来获得最终的分类结果。
    :param inX: 传入需要测试的列表
    :param dataSet: 特征集合
    :param labels: 类别集合
    :param k: 匹配次数
    :return: 返回训练结果，即所属类型
    """

    # 欧式距离公式：d = [(xA0 - xB0)^2 + (xA1 - xB1)^2]^0.5
    dataSetSize = dataSet.shape[0]
    diffMat = tile(inX, (dataSetSize, 1)) - dataSet
    sqDiffMat = diffMat ** 2
    sqDistances = sqDiffMat.sum(axis=1)
    distances = sqDistances ** 0.5
    # 获得排序后各个值在原数组中的索引
    sortedDistIndicies = distances.argsort()
    classCount = {}
    # 选择距离最小的k个点进行统计
    for i in range(k):
        voteIlabel = labels[sortedDistIndicies[i]]
        classCount[voteIlabel] = classCount.get(voteIlabel, 0) + 1
    # 逆序排序，获得票数最多的特征值
    sortedClassCount = sorted(classCount.items(),
                              key=operator.itemgetter(1), reverse=True)
    return sortedClassCount[0][0]


if __name__ == '__main__':
    createDataSet()
