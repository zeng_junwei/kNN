# !/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2020/4/29 9:54
# @Author  : zjw
# @FileName: modifyDataFile.py
# @Software: PyCharm
# @Blog    ：https://www.cnblogs.com/vanishzeng/


def modifyData(fileName):
    file = open(fileName, 'r')
    allStr = []
    for line in file.readlines():
        arr = line.strip().split(' ')
        if arr[13] == '1':
            arr[13] = '0'
        else:
            arr[13] = '1'
        s = ''
        for i in range(14):
            s += arr[i] + '\t'
        s += '\n'
        allStr.append(s)
    file.close()
    newFile = open(fileName, 'w')
    newFile.writelines(allStr)
    newFile.close()


if __name__ == '__main__':
    modifyData('data/heartTest.txt')
    modifyData('data/heartTraining.txt')
