# !/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 2020/4/26 17:21
# @Author  : zjw
# @FileName: generateData.py
# @Software: PyCharm
# @Blog    ：https://www.cnblogs.com/vanishzeng/


from numpy import *


def generateSomeData(fileName, num):
    trainFile = open(fileName, "w")
    for i in range(num):
        fightCount = float(int(random.uniform(0, 101)))
        kissCount = float(int(random.uniform(0, 101)))
        if fightCount > kissCount:
            label = 1  # 表示动作片
        else:
            label = 0  # 表示爱情片
        trainFile.write(str(fightCount) + "\t" + str(kissCount) + "\t" + str(float(label)) + "\n")
    trainFile.close()


if __name__ == '__main__':
    generateSomeData("data/movieTraining.txt", 200)
    generateSomeData("data/movieTest.txt", 100)
