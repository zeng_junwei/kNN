# kNN

#### 介绍
#### 1. kNN一些实例

##### 实例1：[通过简单例子了解kNN算法实例](https://gitee.com/zeng_junwei/kNN/blob/master/kNN_Example/kNN01.py)

##### 实例2：[kNN算法改进约会网站实例代码](https://gitee.com/zeng_junwei/kNN/blob/master/kNN_Example/kNN02.py)

##### 实例3：[通过对kNN算法的理解自己编写关于改进约会网站的代码](https://gitee.com/zeng_junwei/kNN/blob/master/kNN_Example/kNN_Appointment.py)

##### 实例4：[kNN算法解决手写识别系统实例代码](https://gitee.com/zeng_junwei/kNN/blob/master/kNN_Example/kNN03.py)

#### 2. Logistic一些实例

##### 实例1：[从疝气病症预测病马的死亡率](https://gitee.com/zeng_junwei/kNN/blob/master/Logistic_Example/logRegres.py)

##### 实例2：[从打斗数和接吻数预测电影类型](https://gitee.com/zeng_junwei/kNN/blob/master/Logistic_Example/logRegres.py)

##### 实例3：[从心脏检查样本帮助诊断心脏病](https://gitee.com/zeng_junwei/kNN/blob/master/Logistic_Example/logRegres.py)

##### [自己生成生成实例2的数据代码](https://gitee.com/zeng_junwei/kNN/blob/master/Logistic_Example/generateData.py)

##### [修改心脏病的部分数据使满足要求(具体代码)](https://gitee.com/zeng_junwei/kNN/blob/master/Logistic_Example/modifyDataFile.py)
